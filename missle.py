import pygame
import math
import random

vect = pygame.math.Vector2


class Missle(pygame.sprite.Sprite):
    def __init__(self, scene):
        pygame.sprite.Sprite.__init__(self)
        #必不可少的两条代码
        self.image = pygame.image.load('images/missle/missle0.png')
        self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)
        #获取主scene
        self.main_scene = scene
        #初设速度、角度
        self.x_speed = 0.00
        self.y_speed = 2.00
        self.speed = 6
        self.damage = 3
        self.angle = 0
        #默认不旋转
        self.original = self.image

    #会自动调用
    def update(self):
        """更新导弹位置
           旋转图像，计算x,y方向速度
           计算更新后的坐标点
           超出屏幕自动从所有精灵组删除
        """
        #记录原有中心，复杂转换的图像，转换后中心为原中心
        original_center = self.rect.center
        self.image = pygame.transform.rotate(self.original, -self.angle)
        self.rect.center = original_center
        
        #用临时变量来进行degree和 radian 之间的转换
        angle = self.angle * math.pi / 180
        self.x_speed = self.speed * math.cos(angle)
        self.y_speed = self.speed * math.sin(angle)
        self.rect.x += self.x_speed
        self.rect.y += self.y_speed
        #当目标在导弹到达之前没了，改何去何从？？
        #在主函数里，寻找下一个目标，如果没目标，往太空飞
        if not self.rect.colliderect(self.main_scene.get_rect()):
            self.kill()

    def set_target_coordinate(self, target_xy):
        """ 设置打击目标的坐标点
            target_xy   用Vector2建立，例如 target_xy = pygame.math.Vector2(200,400)
            取得自身坐标，取得目标坐标，计算向量差，计算从原点到向量差的点的这角度
            赋值给self.angle，该值单位 degree
        """
        pos0 = vect(0, 0)
        pos1 = vect(self.rect.centerx, self.rect.centery)
        pos2 = target_xy
        pos21 = pos2 - pos1
        self.angle = pos0.angle_to(pos21)

    #为了和飞机的位置配合，需要设置初始位置
    def set_pos(self, x, y):
        self.rect.x = x
        self.rect.y = y

    #为了跟踪弹用的,发现用不着
    def get_pos(self):
        return (self.rect.x, self.rect.y)

    def set_speed(self, speed, angle=0):
        """ 设置速度、角度
            speed 速度
            angle 角度
            用三角函数计算x,y两个方向的度数
        """
        self.speed = speed
        self.angle = angle
        angle = math.pi * angle / 180
        self.x_speed = speed * math.cos(angle)
        self.y_speed = speed * math.sin(angle)
