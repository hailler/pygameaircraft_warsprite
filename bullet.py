"""
子弹的基类
设置坐标，速度，角度，计算更新后的点的坐标
"""
import pygame
import math
from pygame import Vector2
vect = pygame.math.Vector2


class Bullet(pygame.sprite.Sprite):
    def __init__(self, scene):
        pygame.sprite.Sprite.__init__(self)
        #必须要的两个初始化
        self.image = pygame.image.load('images/bullet/bulletdefault.png')
        self.rect = self.image.get_rect()
        #用vector来表述，清晰些，子弹的尺寸，用来飞机发射时的位置
        self.size = vect(self.rect.width, self.rect.height)
        #所有的运算都在主函数的scene里，
        # 所以要通过调用Bullet的类的scene，来获取主函数的scene
        self.main_scene = scene
        #设置图像的mask属性，碰撞检查时，透明图像就忽略了
        self.mask = pygame.mask.from_surface(self.image)
        #设置默认的数据参数，速度
        self.speed = 4
        #笛卡尔坐标系，angle = 90 垂直向下
        self.angle = 90
        #初始距离和原点坐标
        self.distance = 0
        self.x_original = 0
        self.y_original = 0
        #伤害值
        self.damage = 1

    #继承至sprite，被sprite.Group()加入后，只要调用了sprite的update，这里也会自动调用
    def update(self):
        """计算图像位置
            先计算单位时间后的距离，通过三角运算，加上原始坐标，即为更新后坐标。
            想用pygame.time.get_fps()来计算时间，效果不好，直接写成了1000
        """
        #计算距离
        self.distance += 1000 * self.speed / 1000
        #degree 换成 radian 角度换弧度,用临时变量angle，保障self.angle不被变更
        angle = self.angle
        angle = math.pi * angle / 180
        #计算图像的左上角的x,y,三角函数换算加上原始点坐标
        self.rect.x = self.distance * math.cos(angle) + self.x_original
        self.rect.y = self.distance * math.sin(angle) + self.y_original
        # 用一条语句实现了跑出屏幕的检查，我真是佩服我自己
        # 如果子弹和屏幕不相碰，就表示在屏幕外了
        #kill()超出屏幕自动删除，从所有sprite组里删除
        if not self.rect.colliderect(self.main_scene.get_rect()):
            self.kill()

    #为了和飞机的位置配合，需要设置初始位置
    def set_pos(self, x, y):
        """设置位置
           x 左上方x坐标
           y 左上方y坐标
        """
        self.rect.x = x
        self.rect.y = y
        #rect.x,y会通过update更新，所以要记录原始点的坐标，用来计算更新后的坐标
        self.x_original = x
        self.y_original = y

    # 为了获取当前坐标，目前位置没用到
    def get_pos(self):
        """获取坐标,返回X,Y两个值
           x 左上方x坐标
           y 左上方y坐标
        """
        return (self.rect.x, self.rect.y)

    #设置速度、角度
    def set_speed(self, speed = 4, angle=0):
        """设置速度
        speed 速度，为X,Y两个方向的向量和
        angel 角度degree(不是弧度radian)
        """
        self.speed = speed
        self.angle = angle
    
    #设置图像,重新计算rect
    def set_image(self,image):
        """ 设置子弹的图像"""
        self.image = image
        self.rect = self.image.get_rect()
    
    #设置伤害值
    def set_damage(self,damage = 1):
        """ 设置子弹的伤害值 damage int
        """
        self.damage = damage