# pygame 飞机大战 sprite编写

#### Description
用pygame的sprite 重写了飞机大战
实现功能：
英雄机：按住按键连续发射
子弹：各种子弹，静态的，动态的，不同的子弹不同的伤害。
发射方式：花样的发射方式，单、双、三、45度……，各种发射方式。
敌机：不同的敌机，随机的发射方式，随机的子弹，不同的生命值。
BOSS：敌机的属性，不同的运动方式，生命显示条，动态的飞机图片
等级控制：按照英雄的得分，变换敌人强度
碰撞检查：用精灵自带的碰撞检查函数，检查碰撞，实现爆炸效果，实现不同的子弹对敌机造成伤害后，计算敌机的生命值。
声音：背景音乐，发射音效，爆炸音效。
差不多实现了要的功能。
单纯的sprite的group函数管理起大量的精灵还是有点力不从心。例如 BOSS 的生命条，BOSS的动态显示，不能靠group来管理，没有对共性的动态图进行进行进一步抽象，类化。
还得用sprite里的layer概念来管理比较好。

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)