import pygame
import math
import random

vect = pygame.math.Vector2


class Supply(pygame.sprite.Sprite):
    def __init__(self, scene):
        pygame.sprite.Sprite.__init__(self)
        #必不可少的两条代码
        self.image = pygame.image.load('images/supply/supply0.png')
        self.rect = self.image.get_rect()
        #获取主scene
        self.main_scene = scene
        #初设速度、角度
        self.x_speed = 0.00
        self.y_speed = 2.00
        self.speed = 2
        self.angle = 0
     

    #会自动调用
    def update(self):
        self.rect.x += self.x_speed
        self.rect.y += self.y_speed
      

    
    def set_pos(self, x, y):
        self.rect.x = x
        self.rect.y = y

