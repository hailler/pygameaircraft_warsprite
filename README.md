# pygame 飞机大战 sprite编写

#### 介绍
用pygame的sprite 重写了飞机大战
实现功能：
英雄机：按住按键连续发射
子弹：各种子弹，静态的，动态的，不同的子弹不同的伤害。
发射方式：花样的发射方式，单、双、三、45度……，各种发射方式。
敌机：不同的敌机，随机的发射方式，随机的子弹，不同的生命值。
BOSS：敌机的属性，不同的运动方式，生命显示条，动态的飞机图片
等级控制：按照英雄的得分，变换敌人强度
碰撞检查：用精灵自带的碰撞检查函数，检查碰撞，实现爆炸效果，实现不同的子弹对敌机造成伤害后，计算敌机的生命值。
声音：背景音乐，发射音效，爆炸音效。
差不多实现了要的功能。
单纯的sprite的group函数管理起大量的精灵还是有点力不从心。例如 BOSS 的生命条，BOSS的动态显示，不能靠group来管理，没有对共性的动态图进行进行进一步抽象，类化。
还得用sprite里的layer概念来管理比较好。

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)