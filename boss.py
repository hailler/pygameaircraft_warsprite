import pygame
from enemyplane import *
from bossbullet import *
vect = pygame.math.Vector2

class BossPlane(EnemyPlane):
    def __init__(self, scene):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('./images/boss/0.png')

        # 飞机矩形
        self.rect = self.image.get_rect()
        #设置mask属性，解决卡的问题
        self.mask = pygame.mask.from_surface(self.image)
        self.main_scene = scene
        self.size = vect(self.rect.width, self.rect.height)
        # 飞机速度
        self.speed = 2
        self.x_speed = 2
        self.y_speed = 2
        self.angle = 45
        self.bullet_sprite = pygame.sprite.Group()
        # 飞机血量
        self.HP = 100
        self.score = 100
        self.motion_type =0
        self.fire_motion_type = 2
        # 飞机子弹发射间隔 毫秒
        self.interval = 1000
        # 飞机子弹发射当前时间
        self.start_time = pygame.time.get_ticks()
        # BOSS 动态图的刷新频率
        self.last_time = pygame.time.get_ticks()
        #设置boss的动态图

        self.rate = 200
        self.frame = 0
        self.first_frame = 0
        self.last_frame = 0
        self.load_images('images/boss/', 0, 10, '.png')

    def update(self):
        """ 
        设置BOSS的运动方式
            根据self.motion_tpye的值，运动
            0 左右移动
            1 光线反射运动,屏幕上半部
        """
        if self.motion_type == 0:
            if self.rect.x + self.size.x > SCENEWIDTH or self.rect.x < 0:
                self.x_speed = -self.x_speed
            # self.rect.y += self.y_speed
            self.rect.x += self.x_speed
        elif self.motion_type == 1:
            if self.rect.x + self.size.x > SCENEWIDTH or self.rect.x < 0:
                self.x_speed = -self.x_speed
            if self.rect.y + self.size.y > SCENEHEIGHT//2 or self.rect.y < 0:
                self.y_speed = -self.y_speed
            self.rect.x += self.x_speed
            self.rect.y += self.y_speed
        elif self.motion_type == 2:
            pass
        
        self.update_fram()
        if self.HP <= 0:
            self.kill()
        # self.update_life_indicate()

    def set_motion_tpye(self, type=0):
        """ 设置BOSS的运动方式
            根据self.motion_tpye的值，运动
            0 左右移动
            1 光线反射运动
        """

        self.motion_type = type
    def get_pos(self):
        return vect(self.rect.x,self.rect.y)


    def fire(self):

        current_time = pygame.time.get_ticks()
        pass_time = current_time - self.start_time
        if pass_time < self.interval:
            return
        self.start_time = current_time
        # boss_pos = vect(self.rect.x, self.rect.y)
        # bullet = BossBullet(self.main_scene)
        # bullet.set_bullet_type(type = random.randint(0,8))
        # bullet.set_motion_type(type=random.randint(0,8))
        self.bullet_motion_type = random.randint(0,8)
        self.bullet_type = random.randint(3,8)

        if self.bullet_motion_type == 0:
            bullet = BossBullet(self.main_scene)
            bullet.set_bullet_type(type = 0)
            bullet.set_motion_type(type=0)
            for v in bullet.bullet_sprite:
                v.set_pos(self.rect.x + v.size.x // 2,
                      self.rect.y + v.size.y)
            self.bullet_sprite.add(v)
        # 两个
        elif self.bullet_motion_type == 1:
            bullet = BossBullet(self.main_scene)
            bullet.set_bullet_type(random.randint(0,2))
            bullet.set_motion_type(type=1)
            interval = -self.rect.width // 4
            for v in bullet.bullet_sprite:
                # blank = self.size/2
                v.set_pos(self.rect.x - v.size.x // 2 + interval, self.rect.y + v.size.y)
                self.bullet_sprite.add(v)
                interval += self.rect.width // 2

        # 三个
        elif self.bullet_motion_type == 2:
            bullet = BossBullet(self.main_scene)
            bullet.set_bullet_type(random.randint(0,2))
            bullet.set_motion_type(type=2)
            interval = -self.rect.width / 2
            for v in bullet.bullet_sprite:
                v.set_pos(self.rect.x - v.size.x // 2 + interval, self.rect.y)
                self.bullet_sprite.add(v)
                interval += self.rect.width // 2
        #其他的发射以BOSS为中心
        else:
            bullet = BossBullet(self.main_scene)
            bullet.set_bullet_type(self.bullet_type)
            bullet.set_motion_type(self.bullet_motion_type)
            for v in bullet.bullet_sprite:
                v.set_pos(self.rect.x + v.size.x // 2,
                        self.rect.y + v.size.y)
                self.bullet_sprite.add(v)

    def load_images(self, filename_prefix, begin_num, end_num,
                    filename_suffix):
        """ 加载一系列图片
            filename_prefix 图片的路径及图名一致部分,例如 "imagse/blast"

            begin_num 图名的数字编码开始,int 类型   例如 0

            end_num 图名数字编码结束，int 类型     例如 10
            
            filename_suffix 图名的后缀，例如 ".png"
        """
        self.images = [
            pygame.image.load(filename_prefix + str(v) + filename_suffix)
            for v in range(begin_num, end_num + 1)
        ]
        self.image = self.images[0]
        self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)
        self.last_frame = end_num - 1

    def draw_life_indicate(self):
        hp_remain = self.HP/100
        
        if self.HP <= 0:
            self.kill()

        if hp_remain > 0.50:
            # print(hp_remain)
            pygame.draw.line(self.main_scene, (0, 255, 0),
                                (200, 0), (200+300*hp_remain, 0), 30)
        else:
            self.motion_type = 1
            pygame.draw.line(self.main_scene, (255, 0, 0),
                                (200, 0), (200+300*hp_remain, 0), 30)

    def update_fram(self):
        cur_time = pygame.time.get_ticks()
        if cur_time > self.last_time + self.rate:
            self.frame += 1
            if self.frame > self.last_frame:
                self.frame = self.first_frame
            self.last_time = cur_time
            self.image = self.images[self.frame]
