from bullet import *
from pygame import Vector2
vect = pygame.math.Vector2
"""
敌人子弹类的集合，继承至Bullet类
和HeroBullet类一样的实现方式
两个类都编写，是因为混在一起太麻烦了，每个函数都要判断是hero 还是 enemy
"""

class EnemyBullet(Bullet):
    def __init__(self, scene):
        #用bullet的初始化函数，减少速度、角度等初始化设置
        Bullet.__init__(self, scene)
        # pygame.sprite.Sprite.__init__(self)
        #下面两条代码不能少，sprite初始化需要
        self.image = pygame.image.load('images/bullet/enemybullet0.png')
        self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)
       
        self.main_scent = scene
        self.size = vect(self.rect.width, self.rect.height)
        self.motion_type = 0
        self.image_type = 0
        #多个子弹时需要用到Group，用精灵类，爽的不要不要的
        #发现Group.add居然能添加Group，不需要逐个sprite添加。
        self.bullet_sprite = pygame.sprite.Group()

    def set_bullet_type(self,type = 0):
        """设置子弹的类型
           子弹的图片存在同一个目录里，以 enemybullet 加 序号 加 .png
           type 0~8
        """
        self.image_type = type
        image_index = "images/bullet/enemybullet"+str(type)+".png"
        self.image = pygame.image.load(image_index)
        self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)

    #设置子弹的行动方式
    def set_motion_type(self, type=0):
        """设置子弹的发射方式
           0 默认值 单个，向下
           1 两个
           2 三个
           3 45度
           4 60度
           5 90度
           6 前方180度角范围内
           7 前方270度角
           8 四周360度角
        """
        self.motion_type = type
        # 默认方式，向下直射 90度
        if self.motion_type == 0:
            bullet = Bullet(self.main_scene)
            bullet.set_speed(self.speed, 90)
            bullet.image = self.image
            self.bullet_sprite.add(bullet)
        # 两个子弹
        elif self.motion_type == 1:
            for angle in range(0,2):
                bullet = Bullet(self.main_scent)
                bullet.set_speed(self.speed, 90)
                bullet.image = self.image
                self.bullet_sprite.add(bullet)
        # 三个子弹
        elif self.motion_type == 2:
            for angle in range(0,3):
                bullet = Bullet(self.main_scent)
                bullet.set_speed(self.speed, 90)
                bullet.image = self.image
                self.bullet_sprite.add(bullet)
        #向下30度,range不后面的数，所以106后，才能75,85,95,105,115五个
        elif self.motion_type == 3:
            for angle in range(75, 106, 10):
                bullet = Bullet(self.main_scent)
                bullet.set_speed(self.speed, angle)
                bullet.image = self.image
                self.bullet_sprite.add(bullet)
        #向下60度范围直射
        elif self.motion_type == 4:
            for angle in range(60, 121, 10):
                bullet = Bullet(self.main_scent)
                bullet.set_speed(self.speed, angle)
                bullet.image = self.image
                self.bullet_sprite.add(bullet)
        #向下90度四周发散
        elif self.motion_type == 5:
            for angle in range(45, 136, 10):
                bullet = Bullet(self.main_scent)
                bullet.set_speed(self.speed, angle)
                bullet.image = self.image
                self.bullet_sprite.add(bullet)
        #向下180度四周发散
        elif self.motion_type == 6:
            for angle in range(0, 181, 10):
                bullet = Bullet(self.main_scent)
                bullet.set_speed(self.speed, angle)
                bullet.image = self.image
                self.bullet_sprite.add(bullet)
        #向下270度四周发散
        elif self.motion_type == 7:
            for angle in range(-45, 225, 10):
                bullet = Bullet(self.main_scent)
                bullet.set_speed(self.speed, angle)
                bullet.image = self.image
                self.bullet_sprite.add(bullet)
        # 360 度
        elif self.motion_type == 8:
            for angle in range(0, 360, 10):
                bullet = Bullet(self.main_scent)
                bullet.set_speed(self.speed, angle)
                bullet.image = self.image
                self.bullet_sprite.add(bullet)
        else:
            pass
        
