"""
    动态背景图，用两种方式实现，一种是加载多张图片，一种是加载一张图片，按要求进行切割
    都是把图像存到  images 的列表里，每次取两张，取完后从头开始
    精灵类的self.image 是唯一的，但动态背景交接过程会同时存在两张image，无法使用精灵
    使用方法：在这个里的初设函数里直接加在images列表，也可以在主屏幕的函数里，用load_images或者load_image来加载

"""
import pygame
from setting import *

# 地图


class Background(object):
    # 初始化地图
    def __init__(self, scene):
        self.image = pygame.image.load('images/bg/bg.png')
        self.images = []

        # 另外一种加载方式 self.load_images('images/bg/bg',0,4,'.png')
        self.main_scene = scene
        self.cur_index = 0
        self.last_index = 1
        self.first_index = 0
        self.next_index = 1

        columns = self.image.get_rect().width // SCENEWIDTH
        image_height = self.image.get_rect().height
        image_width = self.image.get_rect().width
        self.load_image('images/bg/bg.png', image_width, image_height, 1,
                        columns)
        # print(columns, image_height)
        # 保存场景对象
        # self.main_scene = scene
        # 辅助移动地图
        self.image1_rect = self.images[self.cur_index].get_rect()
        self.image2_rect = self.images[self.next_index].get_rect()
        self.y1 = SCENEHEIGHT - self.image1_rect.height
        self.y2 = self.y1 - self.image2_rect.height

    def load_images(self, filename_prefix, begin_num, end_num,
                    filename_suffix):
        """ 
        加载一系列图片
            filename_prefix 图片的路径及图名一致部分,例如 "imagse/blast"
            begin_num 图名的数字编码开始,int 类型   例如 0
            end_num 图名数字编码结束，int 类型     例如 10
            filename_suffix 图名的后缀，例如 ".png"
        """
        self.images = [
            pygame.image.load(filename_prefix + str(v) + filename_suffix)
            for v in range(begin_num, end_num + 1)
        ]
        self.image = self.images[0]
        self.rect = self.image.get_rect()
        self.last_index = end_num - 1

    def load_image(self, filename, width, height, rows, columns):
        """ 
        加载一张图，按照 rows 和 columns切割成多张
            filename 字符串，整个图名，含路径
            width 图的宽度，像素
            height 图的高度，像素
            rows 想要切割的行数
            columns 想要切割的列数
        """ 
        #单个的图，高度和宽度取整数
        frame_width = width // columns
        frame_height = height // rows
        main_picture = pygame.image.load(filename)
        for row in range(rows):
            for col in range(columns):
                frame = main_picture.subsurface([
                    col * frame_width, row * frame_height, frame_width,
                    frame_height
                ])
                self.images.append(frame)
        self.image = self.images[0]
        self.last_index = rows * columns - 1
        self.rect = self.images[0].get_rect()

    # 计算地图图片绘制坐标
    #y1当前图的y坐标，y2下一张图的y坐标
    def update(self):
        #运动速度，每次加 1
        self.y1 = self.y1 + 1
        self.y2 = self.y2 + 1
        # print(self.last_index)
        #第一张图离开屏幕后，换图，当前图为最后一张时，设置成第一张
        if self.y1 >= SCENEHEIGHT:
            self.cur_index += 1
            self.next_index += 1
            if self.cur_index > self.last_index:
                self.cur_index = self.first_index
            if self.next_index > self.last_index:
                self.next_index = self.first_index

            self.image1_rect = self.images[self.cur_index].get_rect()
            self.image2_rect = self.images[self.next_index].get_rect()
            self.y1 = SCENEHEIGHT - self.image1_rect.height
            self.y2 = self.y1 - self.image2_rect.height

    # 绘制地图的两张图片
    def draw(self):
        self.main_scene.blit(self.images[self.cur_index], (0, self.y1))
        self.main_scene.blit(self.images[self.next_index], (0, self.y2))
