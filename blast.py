""" 动态图的加载类
    用两种方式实现，一种是加载多张图片，一种是加载一张图片，按要求进行切割
    都是把图像存到  images 的列表里，每次取两张，取完后从头开始
    精灵类的self.image 是唯一的，但动态背景交接过程会同时存在两张image，无法使用精灵
    使用方法：在这个里的初设函数里直接加在images列表，也可以在主屏幕的函数里，用load_images或者load_image来加载
"""
import pygame
from pygame.locals import *


class Blast(pygame.sprite.Sprite):
    def __init__(self, scene):
        pygame.sprite.Sprite.__init__(self)
        self.main_scene = scene
        self.image = None
        self.rect = None
        self.frame = 0
        self.first_frame = 0
        self.last_frame = 0
        self.columns = 1
        self.images = []
        self.last_time = pygame.time.get_ticks()
        self.blast_time = pygame.time.get_ticks()
        self.rate = 100
        self.load_images('images/blast/b', 1, 11, '.gif')

    def load_images(self, filename_prefix, begin_num, end_num,
                    filename_suffix):
        """ 加载一系列图片
            filename_prefix 图片的路径及图名一致部分,例如 "imagse/blast"

            begin_num 图名的数字编码开始,int 类型   例如 0

            end_num 图名数字编码结束，int 类型     例如 10
            
            filename_suffix 图名的后缀，例如 ".png"
        """
        self.images = [
            pygame.image.load(filename_prefix + str(v) + filename_suffix)
            for v in range(begin_num, end_num + 1)
        ]
        self.image = self.images[0]
        self.rect = self.image.get_rect()
        self.last_frame = end_num - 1

    def load_image(self, filename, width, height, rows, columns):
        """ 
        加载一张图，按照 rows 和 columns切割成多张
            filename 字符串，整个图名，含路径
            width 图的宽度，像素
            height 图的高度，像素
            rows 想要切割的行数
            columns 想要切割的列数
        """
        frame_width = width // columns
        frame_height = height // rows
        self.boom_picture = pygame.image.load(filename)
        for row in range(rows):
            for col in range(columns):
                frame = self.boom_picture.subsurface([
                    col * frame_width, row * frame_height, frame_width,
                    frame_height
                ])
                self.images.append(frame)
        self.image = self.images[0]
        self.last_frame = rows * columns - 1
        self.rect = self.images[0].get_rect()

    def set_pos(self, x, y):
        """ 设置爆炸效果的 x,y
        """
        self.rect.x = x
        self.rect.y = y

    def update(self):
        current_time = pygame.time.get_ticks()
        if current_time > self.last_time + self.rate:
            self.frame += 1
            if self.frame > self.last_frame:
                self.frame = self.first_frame
            self.last_time = current_time
            self.image = self.images[self.frame]
        #爆炸效果持续 1000毫秒
        if current_time - self.blast_time > 1000:
            self.kill()


# pygame.init()
# screen = pygame.display.set_mode((800, 600), 0, 32)
# pygame.display.set_caption("精灵类测试")
# font = pygame.font.Font(None, 18)
# framerate = pygame.time.Clock()

# blast1 = Blast(screen)
# blast1.load_image('images/blast/blast9.png', 658, 329, 4,8)
# blast2 = Blast(screen)
# blast2.load_images('images/blast/b', 1, 11, '.gif')
# group = pygame.sprite.Group()
# blast1.set_pos(200, 220)
# blast2.set_pos(400,200)
# group.add(blast1)
# group.add(blast2)

# while True:

#     for event in pygame.event.get():
#         if event.type == pygame.QUIT:
#             pygame.quit()
#             exit()
#     key = pygame.key.get_pressed()
#     if key[pygame.K_ESCAPE]:
#         exit()

#     screen.fill((0, 0, 0))

#     group.update()
#     group.draw(screen)
#     pygame.display.update()