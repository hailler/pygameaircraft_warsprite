import pygame
import random
from enemybullet import *
from setting import *

# 敌人飞机


class EnemyPlane(pygame.sprite.Sprite):
    # 初始化敌人飞机
    def __init__(self, scene):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('./images/enemy/enemy0.png')
        self.rect = self.image.get_rect()

        self.size = vect(self.rect.width, self.rect.height)
        self.main_scene = scene
        self.bullet_sprite = pygame.sprite.Group()

        self.plane_type = 0
        self.bullet_motion_type = 0
        self.bullet_type = 0
        self.fire_type = 0
        self.HP = 1
        self.score = 1
        self.speed = 2
        self.x_speed = 0
        self.y_speed = 2
        self.start_time = pygame.time.get_ticks()
        self.interval = 200

    def set_plane_type(self, type=1):
        """ 设置飞机类型

            type  0~6,目前的定义，可以添加

            type 不同，图像，飞行速度，子弹发射时间不同
        """
        self.plane_type = type
        if self.plane_type == 0:
            self.image = pygame.image.load("./images/enemy/enemy0.png")
            # 飞机矩形
            self.rect = self.image.get_rect()
            # 飞机速度
            self.speed = 2
            self.score = 1
            # 子弹间隔
            self.interval = 1000

        elif self.plane_type == 1:
            self.image = pygame.image.load("./images/enemy/enemy1.png")
            # 飞机矩形
            self.rect = self.image.get_rect()
            # 飞机速度
            self.speed = 2
            self.score = 1
            # 子弹间隔
            self.interval = 1500
        elif self.plane_type == 2:
            self.image = pygame.image.load("./images/enemy/enemy2.png")
            # 飞机矩形
            self.rect = self.image.get_rect()

            # 飞机速度
            self.speed = 2
            self.score = 2
            self.interval = 2000
        elif self.plane_type == 3:
            self.image = pygame.image.load("./images/enemy/enemy3.png")
            # 飞机矩形
            self.rect = self.image.get_rect()
            # 飞机速度
            self.speed = 3
            self.score = 3
            self.HP = 10
            self.interval = 1500
        elif self.plane_type == 4:
            self.image = pygame.image.load("./images/enemy/enemy4.png")
            # 飞机矩形
            self.rect = self.image.get_rect()
            self.speed = 3
            self.score = 4
            self.HP = 12
            self.interval = 1500
        elif self.plane_type == 5:
            self.image = pygame.image.load("./images/enemy/enemy5.png")
            # 飞机矩形
            self.rect = self.image.get_rect()
            # 飞机速度
            self.speed = 3
            self.score = 5
            self.HP = 15
            self.interval = 1500
        elif self.plane_type == 6:
            self.image = pygame.image.load("./images/enemy/enemy6.png")
            # 飞机矩形
            self.rect = self.image.get_rect()
            # 飞机速度
            self.speed = 3
            self.score = 5
            self.HP = 15
            self.interval = 1500

        # 默认
        else:
            self.image = pygame.image.load("./images/enemy/enemy0.png")
            # 飞机矩形
            self.rect = self.image.get_rect()

            self.speed = 2
            # 飞机生命
            self.HP = 10
            # 子弹间隔
            self.interval = 1000

    # 设置飞机位置
    def set_pos(self, x, y):
        self.rect.x = x
        self.rect.y = y

    def update(self):
        """ 
        更新位置
            计算位置，精灵组自动调用
            超出屏幕，自毁
        """
        self.rect.x += self.x_speed
        self.rect.y += self.y_speed
        if self.HP <= 0:
            self.kill()
        if not self.rect.colliderect(self.main_scene.get_rect()):
            self.kill()

    def set_bullet_motion_type(self, type=0):
        """ 设置发射类型
        type 0~8     
           0 默认值 单个，向下
           1 两个
           2 三个
           3 45度
           4 60度
           5 90度
           6 前方180度角范围内
           7 前方270度角
           8 四周360度角
        """
        self.bullet_motion_type = type

    def set_bullet_type(self, type=0):
        """ 
        子弹的类型,类型不同，图片不同，攻击力不同

            type 0~8
        """
        self.bullet_type = type

    def set_motion_type(self, type=0):
        """ 
        飞机的飞行类型
             type 0
        """
        self.motion_type = type

    def fire(self):
        """ 
        发射子弹方式
            大于发射间隔，发射子弹
            发射的子弹 根据 bullet_type,fire_type值改变

        """

        current_time = pygame.time.get_ticks()
        pass_time = current_time - self.start_time
        if pass_time < self.interval:
            return
        self.start_time = current_time
        #发射子弹
        # bullets = EnemyBullet(self.main_scene)
        # bullets.set_bullet_type(self.bullet_type)
        # bullets.set_motion_type(self.fire_type)
        # for v in bullets.bullet_sprite:
        #     v.set_pos(self.rect.x + v.size.x // 2,
        #               self.rect.y + bullets.size.y)
        #     self.bullet_sprite.add(v)

        if self.bullet_motion_type == 0:
            bullet = EnemyBullet(self.main_scene)
            bullet.set_bullet_type(self.bullet_type)
            bullet.set_motion_type(type=0)
            for v in bullet.bullet_sprite:
                v.set_pos(self.rect.x + v.size.x // 2,
                      self.rect.y + v.size.y)
            self.bullet_sprite.add(v)
        # 两个
        elif self.bullet_motion_type == 1:
            bullet = EnemyBullet(self.main_scene)
            bullet.set_bullet_type(self.bullet_type)
            bullet.set_motion_type(type=1)
            interval = -self.rect.width // 4
            for v in bullet.bullet_sprite:
                # blank = self.size/2
                v.set_pos(self.rect.x - v.size.x // 2 + interval, self.rect.y + v.size.y)
                self.bullet_sprite.add(v)
                interval += self.rect.width // 2

        # 三个
        elif self.bullet_motion_type == 2:
            bullet = EnemyBullet(self.main_scene)
            bullet.set_bullet_type(self.bullet_type)
            bullet.set_motion_type(type=2)
            interval = -self.rect.width / 2
            for v in bullet.bullet_sprite:
                v.set_pos(self.rect.x - v.size.x // 2 + interval, self.rect.y)
                self.bullet_sprite.add(v)
                interval += self.rect.width // 2
        #发射子弹
        else:
            bullet = EnemyBullet(self.main_scene)
            bullet.set_bullet_type(self.bullet_type)
            bullet.set_motion_type(self.fire_type)
            for v in bullet.bullet_sprite:
                v.set_pos(self.rect.x + v.size.x // 2,
                        self.rect.y + v.size.y)
                self.bullet_sprite.add(v)