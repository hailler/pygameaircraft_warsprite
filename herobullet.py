from bullet import *
from pygame import Vector2
vect = pygame.math.Vector2

"""
英雄子弹类的集合
继承至子弹类,所有子弹存在bullet_sprite里，发射函数在 Hero类里写
set_image_tpye(self, type = 0) 根据type值，设置不同的子弹图片、速度、攻击力
set_motion_type(self, type = 0)根据type值，设置子弹的发射方式
类的使用示例 example
bullet = HeroBullet()
bullet.set_bullet_type(type = 1 )
bullet.set_motion_type(type = 1 )

"""


class HeroBullet(Bullet):
    def __init__(self, scene):
        #用bullet的初始化函数，减少速度、角度等初始化设置
        Bullet.__init__(self, scene)
        #下面两条代码不能少，sprite初始化需要
        self.image = pygame.image.load('images/bullet/herobullet0.png')
        self.rect = self.image.get_rect()

        self.main_scent = scene
        self.size = vect(self.rect.width, self.rect.height)

        self.motion_type = 0
        self.image_type = 0
        self.bullet_type = 0
        self.bullet_damage = 1
        #多个子弹时需要用到Group，用精灵类，爽的不要不要的
        #发现Group.add居然能添加Group，不需要逐个sprite添加。
        self.bullet_sprite = pygame.sprite.Group()
        #跟踪弹，发射方式单一，没写代码
        self.missle_sprite = pygame.sprite.Group()

    def set_bullet_type(self,type = 0):
        """设置子弹的类型
           子弹的图片存在同一个目录里，以 herobullet 加 序号 加 .png

           type 0~3 目前
        """
        # self.image_type = type
        self.bullet_type = type
        image_index = "images/bullet/herobullet"+str(type)+".png"
        #重新加载子弹图片，计算子弹位置
        self.image = pygame.image.load(image_index)
        self.rect = self.image.get_rect()
        # self.damage 可以根据子弹的图片不同，设置不同的值
        self.damage = 1

    #设置子弹的行动方式
    def set_motion_type(self, type = 0):
        """设置子弹的发射方式，子弹的图像,攻击力都存储到sprite里
           0 默认值 单个，向上
           1 两个
           2 三个
           3 前方45度角范围内
           4 前方90度角范围内
           5 前方180度角范围内
           6 四周360度角
        """
        self.motion_type = type
        # 默认方式，向上直射，坐标系里为 -90度，damage后面的后续添加
        if self.motion_type == 0:
            bullet = Bullet(self.main_scene)
            bullet.set_speed(self.speed, 270)
            bullet.set_image(self.image)
            bullet.set_damage(self.damage)

            self.bullet_sprite.add(bullet)
        #两个子弹，向上直射
        elif self.motion_type == 1:
            for v in range(0,2):
                bullet = Bullet(self.main_scene)
                bullet.set_speed(self.speed,270)
                bullet.set_image(self.image)
                self.bullet_sprite.add(bullet)
        # 三个子弹,向上直射
        elif self.motion_type == 2:
            for v in range(0,3):
                bullet = Bullet(self.main_scene)
                bullet.set_speed(self.speed,270)
                bullet.set_image(self.image)
                self.bullet_sprite.add(bullet)

        #上方45度
        elif self.motion_type == 3:
            for angle in range(247, 294, 10):
                bullet = Bullet(self.main_scene)
                bullet.set_speed(self.speed, angle)
                bullet.set_image(self.image)
                self.bullet_sprite.add(bullet)
        #上方 90度
        elif self.motion_type == 4:
            for angle in range(225, 315, 10):
                bullet = Bullet(self.main_scene)
                bullet.set_speed(self.speed, angle)
                bullet.image = self.image
                self.bullet_sprite.add(bullet)
        #上方 180
        elif self.motion_type == 5:
            for angle in range(180, 360, 10):
                bullet = Bullet(self.main_scent)
                bullet.set_speed(self.speed, angle)
                bullet.image = self.image
                self.bullet_sprite.add(bullet)
        # 360 度
        elif self.motion_type == 6:
            for angle in range(0, 360, 10):
                bullet = Bullet(self.main_scent)
                bullet.image = self.image
                bullet.set_speed(self.speed, angle)
                self.bullet_sprite.add(bullet)
        else:
            pass
