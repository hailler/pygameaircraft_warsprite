import pygame
from enemybullet import *


class BossBullet(EnemyBullet):
    def __init__(self, scene):
        EnemyBullet.__init__(self, scene)

        self.rate = 200
        self.frame = 0
        self.first_frame = 0
        self.last_frame = 0
        self.images = []
        self.start_time = pygame.time.get_ticks()
        # self.load_images('images/boss/', 0, 10, '.png')

    def load_images(self, filename_prefix, begin_num, end_num,
                    filename_suffix):
        """ 加载一系列图片
            filename_prefix 图片的路径及图名一致部分,例如 "imagse/blast"

            begin_num 图名的数字编码开始,int 类型   例如 0

            end_num 图名数字编码结束，int 类型     例如 10
            
            filename_suffix 图名的后缀，例如 ".png"
        """
        self.images = [
            pygame.image.load(filename_prefix + str(v) + filename_suffix)
            for v in range(begin_num, end_num + 1)
        ]
        self.image = self.images[0]
        self.rect = self.image.get_rect()
        self.mask = pygame.mask.from_surface(self.image)
        self.last_frame = end_num - 1

    def update(self):
        EnemyBullet.update(self)

        current_time = pygame.time.get_ticks()
        if current_time > self.start_time + self.rate:
            self.frame += 1
            if self.frame > self.last_frame:
                self.frame = self.first_frame
            self.start_time = current_time
            self.image = self.images[self.frame]

    def set_bullet_type(self, type=0):
        """设置子弹的类型
           子弹的图片存在同一个目录里，以 enemybullet 加 序号 加 .png
           type 0~8
        """
        self.image_type = type

        if type == 0:
            self.load_image("images/bullet/bossbullet/bossbullet0.png", 111,
                            47, 1, 1)
        elif type == 1:
            self.load_image("images/bullet/bossbullet/bossbullet1.png", 64,
                            111, 1, 2)
        elif type == 2:
            self.load_image("images/bullet/bossbullet/bossbullet2.png", 60, 93,
                            1, 2)
        else:
            self.load_image("images/bullet/bossbullet/0-" + str(type) + ".png",
                            72, 36, 1, 2)
        # elif type ==3:
        #     self.load_image("images/bullet/bossbullet/0-3.png",72,36,1,2)
        # else:
        #     self.load_image("images/bullet/bossbullet/0-3.png",72,36,1,2)

    def load_image(self, filename, width, height, rows, columns):
        """ 
        加载一张图，按照 rows 和 columns切割成多张
            filename 字符串，整个图名，含路径
            width 图的宽度，像素
            height 图的高度，像素
            rows 想要切割的行数
            columns 想要切割的列数
        """
        frame_width = width // columns
        frame_height = height // rows
        self.boom_picture = pygame.image.load(filename)
        for row in range(rows):
            for col in range(columns):
                frame = self.boom_picture.subsurface([
                    col * frame_width, row * frame_height, frame_width,
                    frame_height
                ])
                self.images.append(frame)
        self.image = self.images[0]
        self.last_frame = rows * columns - 1
        self.rect = self.images[0].get_rect()